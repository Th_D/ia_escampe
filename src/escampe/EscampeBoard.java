package escampe;

/* Please use Junit5 or delete the @Test part*/
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EscampeBoard implements Partie1 {
	private final int LIG = 6;
	private final int COL = 6;
	private final int INIT = 0;
	boolean isInitialB = true;
	boolean isInitialN = true;

	private int lastDestLisere = INIT;
	private String lastMove = "";

	final private static int[][] lisereCase = new int[][] { { 1, 2, 2, 3, 1, 2 }, { 3, 1, 3, 1, 3, 2 },
			{ 2, 3, 1, 2, 1, 3 }, { 2, 1, 3, 2, 3, 1 }, { 1, 3, 1, 3, 1, 2 }, { 3, 2, 2, 1, 3, 2 } };

	enum Side {
		TOP, LEFT, RIGHT, BOTTOM
	}

	class Case {
		int i, j;
		ArrayList<Side> sides = new ArrayList<>();

		Case(String str) {
			i = str.toCharArray()[0] - 'A';
			j = str.toCharArray()[1] - '0' - 1;
			setSides();
		}

		private void setSides() {
			if (i <= 1)
				sides.add(Side.LEFT);
			if (i >= 4)
				sides.add(Side.RIGHT);
			if (j <= 1)
				sides.add(Side.TOP);
			if (i >= 4)
				sides.add(Side.BOTTOM);
		}

		ArrayList<Side> getSides() {
			return sides;
		}

		boolean isFree() {
			return (board[j][i] == '-');
		}

		public String toString() {
			return "" + (char) ('A' + i) + (char) ('0' + j + 1);
		}
	}

	private final static char[] LIGNES = { '1', '2', '3', '4', '5', '6' };
	private final static char[] COLONNES = { 'A', 'B', 'C', 'D', 'E', 'F' };

	private char[][] board = new char[LIG][COL];
	// 'N' : licorne noire ; 'n' : paladin noir ; 'B' : licorne blanche ; 'b' :
	// paladin blanc ; '-' : case vide

	EscampeBoard() {
		initBoard();
	}
	
	private EscampeBoard(char[][] board){
        this.board = new char[6][6];
        for(int i=0; i<6; i++){
            System.arraycopy(board[i],0,this.board[i],0,6);
        }
    }
	
	private void initBoard() {
		for (int i = 0; i < LIG; i++) {
			for (int j = 0; j < COL; j++) {
				board[i][j] = '-';
			}
		}
	}

    public EscampeBoard copy(){
		return new EscampeBoard(this.board);
    }  
	
	int getLastDestLisere() {
		return lastDestLisere;
	}

	@Override
	public void setFromFile(String fileName) {

		try {
			BufferedReader in = new BufferedReader(new FileReader(fileName));
			int cpt = 0;
			int cpt2;

			for (String line = in.readLine(); line != null; line = in.readLine()) {
				if (line.charAt(0) != '%') {
					cpt2 = 0;
					for (int i = 0; i < line.length(); i++) {
						if (line.charAt(i) != ' ' && (line.charAt(i) < '0' || line.charAt(i) > '9')) {
							board[cpt][cpt2] = line.charAt(i);
							cpt2++;
						}
					}
					cpt++;
				}
			}
			in.close();
		} catch (FileNotFoundException e) {
			System.out.println("File Not Found !");
		} catch (IOException e) {
			System.out.println("I/O Exception !");
		}
	}

	@Override
	public void saveToFile(String fileName) {
		// If the file doesn't exists, create and write to it
		// If the file exists, remove all content and write to it
		try (FileWriter writer = new FileWriter(fileName); BufferedWriter bw = new BufferedWriter(writer)) {
			bw.write("%  ABCDEF\n");
			for (int a = 0; a < LIG; a++) {
				bw.write("0" + (a + 1) + " ");
				for (int b = 0; b < COL; b++) {
					bw.write(board[a][b]);
				}
				bw.write(" 0" + (a + 1) + "");
				bw.newLine();
			}
			bw.write("%  ABCDEF\n");
		} catch (IOException e) {
			System.err.format("IOException: %s%n", e);
		}

	}

	public void displayBoard() {
		System.out.println("\n   A B C D E F");
		for (int i = 0; i < LIG; i++) {
			System.out.print("0" + (i + 1) + " ");
			for (int j = 0; j < COL; j++) {
				System.out.print(board[i][j] + " "); // i=ligne, j=colonne
			}
			System.out.println(" 0" + (i + 1));
		}
		System.out.println("   A B C D E F\n");
	}
	
	public char[][] getBoard() {
		return board;
	}
		
	public String getLastMove() {
		return lastMove;
	}

	private boolean isValidClassicalMove(String strMove, String player) {
		String[] parts = strMove.split("-");
		// Mouvement move = new Mouvement(new Case(parts[0]), new Case(parts[1]));
		String dep = parts[0];
		Case caseDep = new Case(dep);
		String[] resArray;
		resArray = possiblesMovesFromCase(dep);

		if (player.toUpperCase().charAt(0) == Character.toUpperCase(board[caseDep.j][caseDep.i])) {
			for (String aResArray : resArray) {
				if (aResArray.equals(strMove)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isValidFirstMove(String strMove, String player) {
		String[] parts = strMove.split("/");
		ArrayList<Case> cases = new ArrayList<>();

		// check if its our turn to init game (1st or 2nd turn)
		char found = '-';
		for (char[] col : board) {
			for (char c : col) {
				if (c != '-') {
					// keep the played side
					if (found == '-')
						found = Character.toUpperCase(c);
					else if (Character.toUpperCase(c) != found)
						return false;
				}
			}
		}

		if (found == Character.toUpperCase(player.toCharArray()[0])) {
			// first move still played
			return false;
		}

		// check if all the selected cells are on the same side
		// (each cells contains 1 or 2 (if corner) sides)
		for (String substr : parts) {
			cases.add(new Case(substr));
		}

		ArrayList<Side> sides = new ArrayList<>();
		for (Case c : cases) {
			// fall here if the wrong side is played
			if (!c.isFree())
				return false;

			if (sides.isEmpty())
				sides.addAll(c.getSides());
			else {
				for (Side s : sides) {
					if (!c.getSides().contains(s))
						sides.remove(s);
				}
				if (sides.isEmpty())
					return false;
			}
		}

		return true;
	}

	/**
	 * indique si le coup <move> est valide pour le joueur <player> sur le plateau
	 * courant
	 * 
	 * @param strMove
	 *            le coup `a jouer, sous la forme "B1-D1" en general, sous la forme
	 *            "C6/A6/B5/D5/E6/F5" pour le coup qui place les pi`eces
	 * @param player
	 *            le joueur qui joue, repr?esent?e par "noir" ou "blanc"
	 */
	@Override
	public boolean isValidMove(String strMove, String player) {

		if (strMove.length() == 17) {
			return isValidFirstMove(strMove, player);
		} else if (strMove.length() == 5) {
			return isValidClassicalMove(strMove, player);
		} else {
			return false;
		}

	}

	@Override
	public String[] possiblesMoves(String player) {
		List<String> res = new ArrayList<>();
		for (int i = 0; i < LIG; i++) {
			for (int j = 0; j < LIG; j++) {
				if ((board[i][j] == player.toLowerCase().charAt(0) || board[i][j] == player.toUpperCase().charAt(0))
						&& ((lisereCase[i][j] == lastDestLisere) || lastDestLisere == 0)) {
					char i_dep = (char) i;
					char j_dep = (char) j;
					j_dep += 'A';
					i_dep += '0' + 1;
					String move = Character.toString(j_dep) + i_dep;
					try {
						res.addAll(Arrays.asList(possiblesMovesFromCase(move)));
					} catch (StackOverflowError e){
						res.clear();
						res.add(possiblesMovesFromCase(move)[0]);
						System.err.println("ON PASSE ICI");
					}

				}
			}
		}

		if (isInitialB && isInitialN) {
			// Je suis le premier � jouer,
			// Fct qui retourne le meilleur placement des pi�ces en d�but de partie
			//this.searchAllPossibilities();
			res.add("F6/F5/E6/E5/D5/B6");  // Pour le moment je me place en bas
		} else if (isInitialB && !isInitialN || !isInitialB && isInitialN) {
			// Je suis le second � jouers
			// Check quel cot� l'adversaire � pris et se positionner sur le cot� oppos�
			boolean isTop = false;
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 2; j++) {
					if (board[i][j] != '-') {
						isTop = true;
						break;
					}
				}
			}
			if (isTop) {
				res.add("F6/F5/E6/E5/D5/B6"); // on se place en bas
				//"F6/F5/E6/D5/B6/B5"
			}
			else {
				res.add("A1/A2/B2/C2/B1/E2"); // on se place en haut
			}
					
		}

		if (res.size() == 0)
			res.add("E"); // the player can't play

		// System.out.println("res of player : " +player + " -> "+res);
		return res.toArray(new String[0]);
	}

	private void searchAllPossibilities() {
		// part_haut contient les 2 lignes du haut = 12 cases
		// part_bas  contient les 2 lignes du bas  = 12 cases
		ArrayList<String> part_haut = new ArrayList<>();
		ArrayList<String> part_bas = new ArrayList<>();
		for (int i = 0; i < LIG; i++) {
			for (int j = 0; j < LIG; j++) {
				char i_dep = (char) i;
				char j_dep = (char) j;
				j_dep += 'A';
				i_dep += '0' + 1;
				String myCase = Character.toString(j_dep) + i_dep;
				if (i < 2)
					part_haut.add(myCase);
				if (i >= 4)
					part_bas.add(myCase);
			}
		}
		
		// return toutes les possibilit�s (11488 resultats au total)
		// Exemple : "C6/A6/B5/D5/E6/F5", "C1/A1/B2/D2/E1/F2", ...
		System.out.println("part_haut " + part_haut);
		ArrayList<String> partB_final;
		partB_final = getInitialPossibilities(part_bas);
		System.out.println("Nb resultat " + partB_final.size());
		
		System.out.println("part_bas  " + part_bas);
		ArrayList<String> partH_final;
		partH_final = getInitialPossibilities(part_haut);
		System.out.println("Nb resultat " +partH_final.size());
		
	}

	private ArrayList<String> getInitialPossibilities(ArrayList<String> myPossibilities) {

		ArrayList<String> initialPossibilities = new ArrayList<>();

		for (int i = 0; i < myPossibilities.size(); i++) {
			ArrayList<String> myPossibilitiesCopie = new ArrayList<>(myPossibilities);
			myPossibilitiesCopie.remove(i);

			String[] result = new String[5];
			String[] cases = new String[myPossibilitiesCopie.size()];
			
			// Get all Paladins cases possibilities
			for (int j = 0; j < myPossibilitiesCopie.size(); j++) {
				cases[j] = myPossibilitiesCopie.get(j);
			}

			Combiner<String> combiner = new Combiner<>(5, cases);
			while (combiner.searchNext(result)) {
				StringBuilder pos; // Licorne
				pos = new StringBuilder(myPossibilities.get(i));
				for (String aResult : result)
					pos.append("/").append(aResult);
				initialPossibilities.add(pos.toString());
			}
		}
		
		return initialPossibilities;
	}

	private static class Combiner<T> {
		int count;
		T[] array;
		int[] indexes;

		Combiner(int count, T[] array) {
			super();
			this.count = count;
			this.array = array;
			indexes = new int[count];
			for (int i = 0; i < count; i++)
				indexes[i] = i;
		}

		boolean searchNext(T[] result) {
			if (indexes == null)
				return false;

			int resultIndex = 0;
			for (int index : indexes)
				result[resultIndex++] = array[index];

			int indexesRank = count - 1;
			int arrayRank = array.length - 1;
			while (indexes[indexesRank] == arrayRank) {
				if (indexesRank == 0) {
					indexes = null;
					return true;
				}
				indexesRank--;
				arrayRank--;
			}

			int restartIndex = indexes[indexesRank] + 1;
			while (indexesRank < count)
				indexes[indexesRank++] = restartIndex++;

			return true;
		}
	}
		

	private boolean canGoTo(Case caseDep, int i, int j, int stepRest, char color) {
		return ((i >= 0 && j >= 0 && i < COL && j < LIG) 
				&& ((board[i][j] == '-') || 
				(stepRest == 1
				&& ( ( ( board[i][j] == 'N') || (board[i][j] == 'B')) && Character.toUpperCase(color) != board[i][j] ) 
				&& !((board[caseDep.j][caseDep.i] == 'N' && board[i][j] == 'B')||(board[caseDep.j][caseDep.i] == 'B' && board[i][j] == 'N') )  ) ) );
	}

	private String[] possiblesMovesFromCase(String pos) {
		List<String> res = new ArrayList<>();
		List<String> resCurr = new ArrayList<>();
		List<String> resOld = new ArrayList<>();
		List<String> resStep1 = new ArrayList<>();
		String[] resArray = new String[0];
		Case caseDep = new Case(pos);

		if (board[caseDep.j][caseDep.i] == '-') {
			return resArray;
		}

		res.add("" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[caseDep.i] + LIGNES[caseDep.j]);

		for (int nbCoupRest = 0; nbCoupRest < lisereCase[caseDep.j][caseDep.i]; nbCoupRest++) {
			for (String coups : res) {

				int i = coups.toCharArray()[3] - 'A';
				int j = coups.toCharArray()[4] - '0' - 1;

				if (canGoTo(caseDep, j, i + 1, lisereCase[caseDep.j][caseDep.i] - nbCoupRest, board[caseDep.j][caseDep.i])) {
					if (!resOld
							.contains("" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[i + 1] + LIGNES[j])
							&& !resCurr.contains(
									"" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[i + 1] + LIGNES[j])) {
						resCurr.add("" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[i + 1] + LIGNES[j]);
					}
				}
				if (canGoTo(caseDep, j, i - 1, lisereCase[caseDep.j][caseDep.i] - nbCoupRest, board[caseDep.j][caseDep.i])) {
					if (!resOld
							.contains("" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[i - 1] + LIGNES[j])
							&& !resCurr.contains(
									"" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[i - 1] + LIGNES[j])) {
						resCurr.add("" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[i - 1] + LIGNES[j]);
					}
				}
				if (canGoTo(caseDep, j + 1, i, lisereCase[caseDep.j][caseDep.i] - nbCoupRest, board[caseDep.j][caseDep.i])) {
					if (!resOld
							.contains("" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[i] + LIGNES[j + 1])
							&& !resCurr.contains(
									"" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[i] + LIGNES[j + 1])) {
						resCurr.add("" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[i] + LIGNES[j + 1]);
					}
				}
				if (canGoTo(caseDep, j - 1, i, lisereCase[caseDep.j][caseDep.i] - nbCoupRest, board[caseDep.j][caseDep.i])) {
					if (!resOld
							.contains("" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[i] + LIGNES[j - 1])
							&& !resCurr.contains(
									"" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[i] + LIGNES[j - 1])) {
						resCurr.add("" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[i] + LIGNES[j - 1]);
					}
				}
			}

			resOld.addAll(resCurr); // On ajoute les resultats obtenus
			res.clear(); // On nettoie le res afin de repartir avec les resultats obtenus pr?cedemment
			res.addAll(resCurr); // On ajout afin de partir des resultats obtenus pr?cedemment
			resCurr.clear();
		}

		if (lisereCase[caseDep.j][caseDep.i] == 3) {

			// voisin bas
			if (isInLimitAndFree(caseDep.j + 1, caseDep.i)
					&& (((isInLimitAndFree(caseDep.j + 1, caseDep.i + 1) && isInLimitAndFree(caseDep.j, caseDep.i + 1)))
							|| (isInLimitAndFree(caseDep.j, caseDep.i - 1)
									&& isInLimitAndFree(caseDep.j + 1, caseDep.i - 1))))
				resStep1.add("" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[caseDep.i]
						+ LIGNES[caseDep.j + 1]);

			// voisin droite
			if (isInLimitAndFree(caseDep.j, caseDep.i + 1)
					&& (((isInLimitAndFree(caseDep.j - 1, caseDep.i) && isInLimitAndFree(caseDep.j - 1, caseDep.i + 1)))
							|| (isInLimitAndFree(caseDep.j + 1, caseDep.i)
									&& isInLimitAndFree(caseDep.j + 1, caseDep.i + 1))))
				resStep1.add("" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[caseDep.i + 1]
						+ LIGNES[caseDep.j]);

			// voisin haut
			if (isInLimitAndFree(caseDep.j - 1, caseDep.i)
					&& (((isInLimitAndFree(caseDep.j, caseDep.i - 1) && isInLimitAndFree(caseDep.j - 1, caseDep.i - 1)))
							|| (isInLimitAndFree(caseDep.j, caseDep.i + 1)
									&& isInLimitAndFree(caseDep.j - 1, caseDep.i + 1))))
				resStep1.add("" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[caseDep.i]
						+ LIGNES[caseDep.j - 1]);

			// voisin gauche
			if (isInLimitAndFree(caseDep.j, caseDep.i - 1)
					&& (((isInLimitAndFree(caseDep.j + 1, caseDep.i) && isInLimitAndFree(caseDep.j + 1, caseDep.i - 1)))
							|| (isInLimitAndFree(caseDep.j - 1, caseDep.i)
									&& isInLimitAndFree(caseDep.j - 1, caseDep.i - 1))))
				resStep1.add("" + COLONNES[caseDep.i] + LIGNES[caseDep.j] + "-" + COLONNES[caseDep.i - 1]
						+ LIGNES[caseDep.j]);
		}

		// System.out.println("possiblesMovesFromCase : "+pos+" return : "+res);
		res.addAll(resStep1);
		resArray = res.toArray(new String[0]);
		return resArray;
	}

	private boolean isInLimitAndFree(int i, int j) {

		if (i >= 0 && i < COL && j >= 0 && j < LIG) {
			return board[i][j] == '-';
		} else {
			return false;
		}

	}

	@Override
	public void play(String move, String player) {
		if (move.length() == 17) {
			/*
			 * init board C6/A6/B5/D5/E6/F5
			 */
			
			if (player.equals("blanc"))
				this.isInitialB = false;
			if (player.equals("noir"))
				this.isInitialN = false;
			
			String[] parts = move.split("/");
			int j_dep = parts[0].toCharArray()[0] - 'A';
			int i_dep = parts[0].toCharArray()[1] - '0' - 1;
			board[i_dep][j_dep] = player.toUpperCase().charAt(0);
			for (int i = 1; i < 6; i++) {
				board[parts[i].toCharArray()[1] - '0' - 1][parts[i].toCharArray()[0] - 'A'] = player.toLowerCase()
						.charAt(0);
			}

		} else if (move.length() == 5) {
			/*
			 * simple move "C1-D1"
			 */
			String[] parts = move.split("-");
			board[parts[1].toCharArray()[1] - '0' - 1][parts[1].toCharArray()[0]
					- 'A'] = board[parts[0].toCharArray()[1] - '0' - 1][parts[0].toCharArray()[0] - 'A'];
			board[parts[0].toCharArray()[1] - '0' - 1][parts[0].toCharArray()[0] - 'A'] = '-';
			lastDestLisere = lisereCase[parts[1].toCharArray()[1] - '0' - 1][parts[1].toCharArray()[0] - 'A'];
		}
		this.lastMove = move;
	}

	// si il manque au moins une licorne -> fin de partie
	@Override
	public boolean gameOver() {
		boolean foundB = false;
		boolean foundN = false;

		for (int a = 0; a < LIG; a++) {
			for (int b = 0; b < COL; b++) {
				if (board[a][b] == 'N')
					foundN = true;
				if (board[a][b] == 'B')
					foundB = true;
			}
		}
		return !(foundB && foundN);
	}

	public static void main(String[] args) {
		System.out.println("RUN Project");
		EscampeBoard escBoard = new EscampeBoard();
		escBoard.setFromFile("dataIn.txt");
		escBoard.saveToFile("dataOut.txt");

		// Coups possible � partir d'une case
		System.out.println("\n[X] possiblesMovesFromCase ");
		String myCase = "D5";
		String[] resArray;
		resArray = escBoard.possiblesMovesFromCase(myCase);
		System.out.println("Les mouvements possibles a partir de la case " + myCase + " sont :");
		for (String possiblesMoves : resArray) {
			System.out.print(possiblesMoves + " ");
		}
		System.out.println("\n\n'D5-F4' is valid move ? " + escBoard.isValidMove("D5-F4", "noir"));
		System.out.println("'D5-A1' is valid move ? " + escBoard.isValidMove("D5-A1", "noir"));

		escBoard.displayBoard();
		// Coups possible pour un joueur
		System.out.println("\n[X] possiblesMoves ");
		String[] resArray2;
		String myPlayerB = "blanc";
		resArray2 = escBoard.possiblesMoves(myPlayerB);
		System.out.println("Les mouvements possibles pour le joueur " + myPlayerB + " sont :");
		for (String possiblesMovesPlayer : resArray2) {
			System.out.print(possiblesMovesPlayer + " ");
		}
		String myPlayerN = "noir";
		String[] resArray3;
		resArray3 = escBoard.possiblesMoves(myPlayerN);
		System.out.println("\nLes mouvements possibles pour le joueur " + myPlayerN + " sont :");
		for (String possiblesMovesPlayer : resArray3) {
			System.out.print(possiblesMovesPlayer + " ");
		}

		EscampeBoard emptyBoard = new EscampeBoard();
		System.out.println("\nDisposition des pi�ces noires en d�but de partie : ");
		emptyBoard.play("C6/A6/B5/D5/E6/F5", "noir");
		emptyBoard.displayBoard();
		System.out.println("Jouer A6-B4 (play) : ");
		emptyBoard.play("A6-B4", "noir");
		emptyBoard.displayBoard();

		emptyBoard.isValidMove("D5-F4", "black");

		resArray3 = emptyBoard.possiblesMoves(myPlayerB);
		System.out.println("\nLes mouvements possibles pour le joueur " + myPlayerB + " sont :");
		for (String possiblesMovesPlayer : resArray3) {
			System.out.print(possiblesMovesPlayer + " ");
		}
		System.out.println("\n\nSEARCH ALL POSSIBILITIES");
		escBoard.searchAllPossibilities();
	}

//	@Test
//	final void testAll() {
//
//		char[][] array_initial = new char[][] { { '-', '-', '-', '-', '-', '-' }, { '-', '-', '-', '-', '-', '-' },
//				{ '-', '-', '-', '-', '-', '-' }, { '-', '-', '-', '-', '-', '-' }, { '-', '-', '-', '-', '-', '-' },
//				{ '-', '-', '-', '-', '-', '-' } };
//		char[][] array_PartieEnCours = new char[][] { { 'b', 'b', '-', '-', '-', '-' },
//				{ '-', 'B', 'b', '-', 'b', 'b' }, { '-', '-', '-', '-', '-', '-' }, { '-', '-', '-', '-', '-', '-' },
//				{ '-', 'n', '-', 'n', '-', 'n' }, { 'n', '-', 'N', '-', 'n', '-' } };
//
//		char[][] array_finPartie = new char[][] { { 'b', 'b', '-', '-', '-', '-' }, { '-', 'B', 'b', '-', 'b', 'b' },
//				{ '-', '-', '-', '-', '-', '-' }, { '-', '-', '-', '-', '-', '-' }, { '-', 'n', '-', 'n', '-', 'n' },
//				{ 'n', '-', '-', '-', 'n', '-' } };
//
//		String filename_in = "dataIn.txt";
//		String filename_out = "dataOut.txt";
//
//		// Creation du plateau de jeu - On verifie que le board a bien ete initialise
//		EscampeBoard escBoard = new EscampeBoard();
//		Assertions.assertArrayEquals(array_initial, board);
//
//		// Update plateau de jeu a partir d'un fichier texte
//		// On verifie que le fichier existe bien et qu'on arrive a le lire
//		// On verifie que le board a ete mis a jour
//		escBoard.setFromFile(filename_in);
//		try {
//			BufferedReader in = new BufferedReader(new FileReader(filename_in));
//			in.close();
//		} catch (IOException e) {
//			Assert.fail("La methode 'setFromFile' ne fonctionne pas correctement");
//		}
//		Assert.assertNotEquals(array_initial, board);
//
//		// Sauvegarde du plateau de jeu dans un fichier
//		// On verifie que le fichier est cree
//		escBoard.saveToFile(filename_out);
//		try {
//			BufferedWriter writer = new BufferedWriter(new FileWriter(filename_out, true));
//			writer.close();
//		} catch (IOException e) {
//			Assert.fail("La methode 'saveToFile' ne fonctionne pas correctement");
//		}
//
//		
//		// Recuperer les mouvements possibles a partir d'une case
//		// On s'attend a ce que le tableau obtenu soit equivalent a myArray1
//		String[] myArray1 = { "D5-F4", "D5-D4", "D5-E3", "D5-B4", "D5-C3" };
//		String[] myArray2 = escBoard.possiblesMovesFromCase("D5");
//		Assertions.assertArrayEquals(myArray1, myArray2);
//
//		// Verifier qu'un coup est valide
//		// c.a.d que le coup soit dans le tableau obtenu par 'possiblesMovesFromCase'
//		Assert.assertTrue(escBoard.isValidMove("D5-F4", "noir"));
//		Assert.assertFalse(escBoard.isValidMove("D5-A1", "noir"));
//
//		// Verifier le mouvement des pions sur le plateau 'play'
//		EscampeBoard emptyBoard = new EscampeBoard();
//		// emptyBoard.displayBoard();
//		Assertions.assertArrayEquals(array_initial, board);
//		emptyBoard.play("C6/A6/B5/D5/E6/F5", "noir");
//		// emptyBoard.displayBoard();
//		Assert.assertNotEquals(array_initial, board);
//		char case1 = emptyBoard.board[5][0]; // A6
//		char case2 = emptyBoard.board[3][1]; // B4
//
//		emptyBoard.play("A6-B4", "noir");
//		char case3 = emptyBoard.board[5][0]; // A6
//		char case4 = emptyBoard.board[3][1]; // B4
//		Assert.assertNotEquals(case1, case2);
//		Assert.assertNotEquals(case3, case4);
//		Assert.assertEquals(case1, case4); // 'n'
//		Assert.assertEquals(case2, case3); // '-'
//
//		// test fin de partie
//		emptyBoard.board = array_PartieEnCours;
//		Assert.assertFalse(emptyBoard.gameOver());
//		emptyBoard.board = array_finPartie;
//		Assert.assertTrue(emptyBoard.gameOver());
//		System.out.println("Tests OK");
//		
//	}

}
