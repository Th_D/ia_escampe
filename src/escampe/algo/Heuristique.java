package escampe.algo;

import escampe.EscampeBoard;
import escampe.JoueurSuperFort;

public class Heuristique {

	private JoueurSuperFort joueurMax;

	Heuristique(JoueurSuperFort joueurMax) {
		this.joueurMax = joueurMax;
	}

	int eval(JoueurSuperFort jsf, EscampeBoard board) {
		int dist_calc;
		int dist_max = 10;
		int dist_min = 10;

		char[][] copieBoard = board.getBoard();
		String licorne_E = jsf.localiseLicorneEnnemi(jsf);
		String current_P;

		if (board.gameOver()) {
			if (jsf.couleur.equals(this.joueurMax.couleur)) {
				return Integer.MIN_VALUE;
			}
			return Integer.MAX_VALUE;
		}

		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				if (copieBoard[i][j] == jsf.couleur.charAt(0)) {
					current_P = ("" + (char) ('A' + j) + (char) ('0' + i + 1));
					dist_calc = jsf.calculate_dist_paladin_LicorneEnnemi(current_P, licorne_E);
					dist_min = Math.min(dist_min, dist_calc);

				}
			}
		}
		return dist_max - dist_min;
	}

}