package escampe.algo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import escampe.EscampeBoard;
import escampe.JoueurSuperFort;

public class MinimaxV2 {

	private JoueurSuperFort joueurMin; // Ennemi
	private JoueurSuperFort joueurMax; // Nous
	private Heuristique h;


	public MinimaxV2(JoueurSuperFort joueurMax) {
		JoueurSuperFort joueurMin = new JoueurSuperFort();
		joueurMin.couleur = joueurMax.couleur.equals("blanc") ? "noir" : "blanc";
		joueurMin.joueur = joueurMax.joueur == -1 ? 1 : -1;
		joueurMin.myBoard = joueurMax.myBoard;
		this.joueurMin = joueurMin;
		this.joueurMax = joueurMax;
		this.h = new Heuristique(joueurMax);
	}

	public String meilleurCoup(EscampeBoard board) {
		String[] array = board.possiblesMoves(this.joueurMax.couleur);
		List<String> coupsPossibles = new ArrayList<>();
		Collections.addAll(coupsPossibles, array);
		
		if (coupsPossibles.size() == 0 || coupsPossibles.get(0).equals("E")) {
			return "E";
		}
		
		if (coupsPossibles.size() == 1) {
			return coupsPossibles.get(0);
		}
		
		if (coupsPossibles.size() > 10 || board.getLastMove().equals("E")) {
			int indice = 0;
			int maxValue = 0;
			for (int i = 0; i < array.length; i++) {
				String destPal = array[i].substring(3, 5);
				int eval = this.joueurMax.eval_heuristique(this.joueurMax, destPal);
				if (eval > maxValue) {
					maxValue = eval;
					indice = i;
				}
			}
			return (array[indice]);
		}
		
		return coupsPossibles.stream().max(Comparator.comparing(move -> {
			EscampeBoard pCopy = simulateBoard(board, this.joueurMax.couleur, move);
			return minmax(pCopy, 1);
		})).orElse("E BUG"); // Ne doit pas passer ici

	    
	  }

	private int minmax(EscampeBoard board, int profondeur) {
		String[] array = board.possiblesMoves(this.joueurMin.couleur);
		List<String> coupsPossibles = new ArrayList<>();
		Collections.addAll(coupsPossibles, array);

		if (board.gameOver() || profondeur > 3) {
			return this.h.eval(this.joueurMax, board);
		}

		int pire = Integer.MAX_VALUE;

		for (String move : coupsPossibles) {
			// <!> Joue le coup sur une copie du board
			EscampeBoard bCopy = board.copy();
			bCopy.play(move, this.joueurMin.couleur);
			pire = Math.min(pire, maxmin(bCopy, profondeur++));
		}

		return pire;
	}

	private int maxmin(EscampeBoard board, int profondeur) {

		String[] array = board.possiblesMoves(this.joueurMax.couleur);
		List<String> coupsPossibles = new ArrayList<>();
		Collections.addAll(coupsPossibles, array);

		if (board.gameOver() || profondeur > 3) {
			return this.h.eval(this.joueurMax, board);
		}

		int meilleur = Integer.MIN_VALUE;

		for (String move : coupsPossibles) {
			// <!> Joue le coup sur une copie du board
			EscampeBoard bCopy = board.copy();
			bCopy.play(move, this.joueurMax.couleur);
			meilleur = Math.max(meilleur, minmax(bCopy, profondeur++));
		}

		return meilleur;
	}

	private EscampeBoard simulateBoard(EscampeBoard board, String player, String move) {
		EscampeBoard bCopy = board.copy();
		bCopy.play(move, player);
		return bCopy;
	}
}
