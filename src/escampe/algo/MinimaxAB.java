package escampe.algo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import escampe.EscampeBoard;
import escampe.JoueurSuperFort;

public class MinimaxAB {

	private Heuristique h;
	private JoueurSuperFort joueurMin; // Ennemi
	private JoueurSuperFort joueurMax; // Nous
	private int profMax;

	public MinimaxAB(JoueurSuperFort joueurMax, int profMax) {
		JoueurSuperFort joueurMin = new JoueurSuperFort();
		joueurMin.couleur = joueurMax.couleur.equals("blanc") ? "noir" : "blanc";
		joueurMin.joueur = joueurMax.joueur == -1 ? 1 : -1;
		joueurMin.myBoard = joueurMax.myBoard;
		this.joueurMin = joueurMin;
		this.joueurMax = joueurMax;

		this.profMax = profMax;
		this.h = new Heuristique(joueurMax);
	}

	public String meilleurCoup(EscampeBoard board) {
		String[] array = board.possiblesMoves(this.joueurMax.couleur);
		List<String> coupsPossibles = new ArrayList<>();
		Collections.addAll(coupsPossibles, array);
		
		System.out.println("meilleurCoup with "+coupsPossibles.size()+ " size ==> " + coupsPossibles);
		if (coupsPossibles.size() == 0 || coupsPossibles.get(0).equals("E")) {
			return "E";
		}
		
		if (coupsPossibles.size() == 1) {
			return coupsPossibles.get(0);
		}
		
		if (coupsPossibles.size() > 10 || board.getLastMove().equals("E")) {
			int indice = 0;
			int maxValue = 0;
			for (int i = 0; i < array.length; i++) {
				String destPal = array[i].substring(3, 5);
				int eval = this.joueurMax.eval_heuristique(this.joueurMax, destPal);
				if (eval > maxValue) {
					maxValue = eval;
					indice = i;
				}
			}
			return (array[indice]);
		}

		return coupsPossibles.stream().max(Comparator.comparing(move -> {
			EscampeBoard bCopy = simulateBoard(board, this.joueurMax.couleur, move);
			return minmaxAB(bCopy, -9999, 9999, 1);
		})).orElse("E BUG"); // on ne doit jamais ici

	}

	private int minmaxAB(EscampeBoard board, int alpha, int beta, int profondeur) {

		String[] array = board.possiblesMoves(this.joueurMin.couleur);
		List<String> coupsPossibles = new ArrayList<>();
		Collections.addAll(coupsPossibles, array);

		if (board.gameOver() || profondeur > profMax) {
			return this.h.eval(this.joueurMin, board);
		}

		for (String move : coupsPossibles) {
			EscampeBoard bCopy = simulateBoard(board, this.joueurMin.couleur, move);
			beta = Math.min(beta, maxminAB(bCopy, alpha, beta, profondeur++));
			if (alpha >= beta)
				return alpha;
		}
		return beta;
	}

	private int maxminAB(EscampeBoard board, int alpha, int beta, int profondeur) {
		String[] array = board.possiblesMoves(this.joueurMax.couleur);
		List<String> coupsPossibles = new ArrayList<>();
		Collections.addAll(coupsPossibles, array);

		if (board.gameOver() || profondeur > profMax) {
			return this.h.eval(this.joueurMax, board);
		}

		for (String move : coupsPossibles) {
			EscampeBoard bCopy = simulateBoard(board, this.joueurMax.couleur, move);
			alpha = Math.max(alpha, minmaxAB(bCopy, alpha, beta, profondeur++));
			if (alpha >= beta)
				return beta;
		}

		return alpha;
	}

	private EscampeBoard simulateBoard(EscampeBoard board, String player, String move) {
		EscampeBoard bCopy = board.copy();
		bCopy.play(move, player);
		return bCopy;
	}

}
