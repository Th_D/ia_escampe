package escampe.algo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import escampe.EscampeBoard;
import escampe.JoueurSuperFort;

public class Minimax {
	
	private Heuristique h;
	private JoueurSuperFort joueurMin; // Ennemi
	private JoueurSuperFort joueurMax; // Nous
	private int profMax;

	public Minimax(JoueurSuperFort joueurMax, int profMaxi) {
		JoueurSuperFort joueurMin = new JoueurSuperFort();
		joueurMin.couleur = joueurMax.couleur.equals("blanc") ? "noir" : "blanc";
		joueurMin.joueur = joueurMax.joueur == -1 ? 1 : -1;
		joueurMin.myBoard = joueurMax.myBoard;
		this.joueurMin = joueurMin;
		this.joueurMax = joueurMax;
		this.profMax = profMaxi;
		this.h = new Heuristique(joueurMax);
	}

	public String meilleurCoup(EscampeBoard board) {
		// System.out.println("Je suis le joueur " + this.joueurMax.couleur);

		String[] array = board.possiblesMoves(this.joueurMax.couleur);
		List<String> coupsPossibles = new ArrayList<>();
		Collections.addAll(coupsPossibles, array);

		System.out.println(coupsPossibles.size());

		if (coupsPossibles.size() > 0 && coupsPossibles.get(0).equals("E")) {
			return "E";
		}

		if (coupsPossibles.size() == 1) {
			return coupsPossibles.get(0);
		}

		if (coupsPossibles.size() > 10 || board.getLastMove().equals("E")) {
			int indice = 0;
			int maxValue = 0;
			for (int i = 0; i < array.length; i++) {
				String destPal = array[i].substring(3, 5);
				int eval = this.joueurMax.eval_heuristique(this.joueurMax, destPal);
				if (eval > maxValue) {
					maxValue = eval;
					indice = i;
				}
			}
			return (array[indice]);
		}

		return coupsPossibles.stream()
				.max(Comparator.comparing(coup -> minMaxforMove(coup, board, this.joueurMax, this.profMax - 1)))
				.orElse(null);
	}

	private int minMax(EscampeBoard board, JoueurSuperFort joueur, int profondeur) {
		String[] array = joueur.myBoard.possiblesMoves(joueur.couleur);
		List<String> coupsPossibles = new ArrayList<>();
		Collections.addAll(coupsPossibles, array);

		if (board.gameOver() || profondeur <= 0) {
			return (this.h.eval(joueur, board));
		}

		Stream<Integer> coupsEvaluation = coupsPossibles.stream()
				.map(coup -> minMaxforMove(coup, board, joueur, profondeur - 1));

		return (profondeur % 2 == 0)
				? coupsEvaluation.max(Comparator.comparing(Integer::valueOf)).orElse(Integer.MIN_VALUE)
				: coupsEvaluation.min(Comparator.comparing(Integer::valueOf)).orElse(Integer.MAX_VALUE);
	}

	private int minMaxforMove(String move, EscampeBoard board, JoueurSuperFort joueur, int profondeur) {
		EscampeBoard bCopy = simulateBoard(board, joueur.couleur, move);
		JoueurSuperFort nextJoueur = joueur.equals(this.joueurMax) ? this.joueurMin : this.joueurMax;
		return minMax(bCopy, nextJoueur, profondeur);
	}
	
	private EscampeBoard simulateBoard(EscampeBoard board, String player, String move) {
		EscampeBoard bCopy = board.copy();
		bCopy.play(move, player);
		return bCopy;
	}

}
