package escampe;

import java.util.Random;

public class JoueurAleatoire implements IJoueur {
	String couleur = "";
	int joueur = 0;
	EscampeBoard myBoard;

	@Override
	public void initJoueur(int mycolour) {
		this.couleur = mycolour == BLANC ? "blanc" : "noir";
		this.joueur = mycolour;
		this.myBoard = new EscampeBoard();
	}

	@Override
	public int getNumJoueur() {
		return this.joueur;
	}

	public String getJoueurByNum(int num) {
		return num == BLANC ? "blanc" : "noir";
	}
	
	public String display_tab(String[] array) {
		String res = "";
		for (int i=0; i< array.length - 1; i++) {
			res += array[i] + " | ";
		}
		res += array[array.length-1];
		return res;
	}

	@Override
	public String choixMouvement() {
		System.out.println("\nC'est � moi de jouer !");
		String[] array = myBoard.possiblesMoves(this.couleur);
		System.out.println(array.length + " coups possibles : " + display_tab(array));
		// IA � faire pour choisir le meilleur mouvemenet possible

		int rnd = new Random().nextInt(array.length);
		this.myBoard.play(array[rnd], this.couleur);
		if (this.couleur == "blanc")
			myBoard.isInitialB = false;
		if (this.couleur == "noir")
			myBoard.isInitialN = false;
		System.out.println("Je choisis de joueur : " + array[rnd]);
		return array[rnd];
	
	}

	@Override
	public void declareLeVainqueur(int colour) {
		System.out.println("\n**************************************************");
		System.out.println("**********" + "  VICTOIRE DU JOUEUR " + getJoueurByNum(colour) + "  ************");
		System.out.println("**************************************************\n");

	}

	@Override
	public void mouvementEnnemi(String coup) {
		this.myBoard.play(coup, this.couleur == "blanc" ? "noir" : "blanc"); // TOVERIF
		if (this.couleur == "blanc")
			myBoard.isInitialN = false;
		if (this.couleur == "noir")
			myBoard.isInitialB = false;
	}

	@Override
	public String binoName() {
		return "Team Thibault & Dorian";
	}

//	 public static void main(String[] args) {
//	 JoueurAleatoire ja = new JoueurAleatoire();
//	 ja.initJoueur(BLANC);
//	 System.out.println("getNumJoueur => " + ja.getNumJoueur());
//	 ja.myBoard.play("C6/A6/B5/D5/E6/F5", "blanc");
//	 ja.myBoard.displayBoard();
//	 System.out.println("ChoixMouvement => " +ja.choixMouvement());
//	 ja.declareLeVainqueur(BLANC);
//	 System.out.println(ja.binoName());
//	 }
}
