package escampe;

public class JoueurFort implements IJoueur {
	public String couleur = "";
	public int joueur = 0;
	private EscampeBoard myBoard;
	
	@Override
	public void initJoueur(int mycolour) {
		this.couleur = mycolour == BLANC ? "blanc" : "noir";
		this.joueur = mycolour;
		this.myBoard = new EscampeBoard();
	}

	@Override
	public int getNumJoueur() {
		return this.joueur;
	}

	private String getJoueurByNum(int num) {
		return num == BLANC ? "blanc" : "noir";
	}

	private String display_tab(String[] array) {
		StringBuilder res = new StringBuilder();
		for (int i = 0; i < array.length - 1; i++) {
			res.append(array[i]).append(" | ");
		}
		res.append(array[array.length - 1]);
		return res.toString();
	}

	
	@Override
	public String choixMouvement() {
		System.out.println("\nC'est � moi de jouer !");
		int indice = 0;
		String[] array = myBoard.possiblesMoves(this.couleur);
		System.out.println(array.length + " coups possibles : " + display_tab(array));
		// Choix du meilleur coup possible == se rapprocher le plus proche de la licorne adverse
		if (!myBoard.isInitialB && !myBoard.isInitialN && !array[0].equals("E")) {
			int maxValue = 0;
			for (int i = 0; i < array.length; i++) {
				String destPal = array[i].substring(3,5);
				int eval = eval_heuristique(this, destPal);
				if (eval > maxValue) {
					maxValue = eval;
					indice = i;
				}
			}
		}

		this.myBoard.play(array[indice], this.couleur);
		System.out.println("Je choisis de joueur : " + array[indice]);
		return array[indice];
	}
		

	private int eval_heuristique(JoueurFort jsf, String myMove) {
		int dist_max = 10;
		int dist_PtoL;
		int h;

		// Localiser la Licorne adverse
		String licorne_E = localiseLicorneEnnemi(jsf);
		
		// Get distance between paladin & licorne adverse
		assert licorne_E != null;
		dist_PtoL = calculate_dist_paladin_LicorneEnnemi(myMove, licorne_E);
		
		// Heuristique = distance_max - distance entre mon paladin et licorne advserse
		h = dist_max - dist_PtoL;
		
		return h;
	}

	private String localiseLicorneEnnemi(JoueurFort jsf) {
		char[][] board = jsf.myBoard.getBoard();
		char licorneEnnemi = this.couleur.equals("blanc") ? 'N' : 'B';
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				if (board[i][j] == licorneEnnemi) {
					return ("" + (char) ('A' + j) + (char) ('0' + i + 1));
				}
			}
		}
		return null;
	}

	private int calculate_dist_paladin_LicorneEnnemi(String case_p, String case_lic) {
		int pal_i = case_p.toCharArray()[0] - 'A';
		int pal_j = case_p.toCharArray()[1] - '0' - 1;
		int lic_i = case_lic.toCharArray()[0] - 'A';
		int lic_j = case_lic.toCharArray()[1] - '0' - 1;		
		// Manhattan distance
		return (Math.abs(pal_i - lic_i) + Math.abs(pal_j - lic_j));
	}

	@Override
	public void declareLeVainqueur(int colour) {
		System.out.println("\n**************************************************");
		System.out.println("**********" + "  VICTOIRE DU JOUEUR " + getJoueurByNum(colour).toUpperCase() + "  ************");
		System.out.println("**************************************************\n");
	}

	@Override
	public void mouvementEnnemi(String coup) {
		System.out.println("Le joueur advserse joue " + coup);
		this.myBoard.play(coup, this.couleur.equals("blanc") ? "noir" : "blanc"); // TOVERIF
	}

	@Override
	public String binoName()
	{
		return "Team Thibault Delorme & Dorian Verriere";
	}

	public static void main(String[] args) {

		JoueurFort jsf_b = new JoueurFort();
		JoueurFort jsf_n = new JoueurFort();
		jsf_b.initJoueur(BLANC);
		jsf_n.initJoueur(NOIR);
		
		jsf_n.myBoard.play("A1/A2/B2/C2/D2/E2", "noir"); // Noir
		jsf_n.myBoard.displayBoard();
		
		jsf_n.mouvementEnnemi("A6/B5/C5/D5/E5/F5"); // Blanc
		jsf_n.myBoard.displayBoard();	

		jsf_n.mouvementEnnemi("A6-B4"); // Blanc
		jsf_n.myBoard.displayBoard();
		
		jsf_n.choixMouvement(); // Noir
		jsf_n.myBoard.displayBoard();
		
		jsf_n.mouvementEnnemi("D5-E3"); // Blanc
		jsf_n.myBoard.displayBoard();
		
		jsf_n.choixMouvement(); // Noir
		jsf_n.myBoard.displayBoard();
		
		jsf_n.mouvementEnnemi("F5-E4"); // Blanc
		jsf_n.myBoard.displayBoard();
		
		jsf_n.choixMouvement(); // Noir
		jsf_n.myBoard.displayBoard();
		
		jsf_b.declareLeVainqueur(NOIR);

	}

}
