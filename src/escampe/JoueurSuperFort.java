package escampe;

import escampe.algo.Minimax;
import escampe.algo.MinimaxAB;
import escampe.algo.MinimaxV2;

public class JoueurSuperFort implements IJoueur {
	public String couleur = "";
	public int joueur = 0;
	public EscampeBoard myBoard;
	private Minimax minimax;
	private MinimaxAB minimaxAB;
	private MinimaxV2 minimaxv2;

	@Override
	public void initJoueur(int mycolour) {
		this.couleur = mycolour == BLANC ? "blanc" : "noir";
		this.joueur = mycolour;
		this.myBoard = new EscampeBoard();
		this.minimax = new Minimax(this, 7);
		this.minimaxAB = new MinimaxAB(this, 1);
		this.minimaxv2 = new MinimaxV2(this);
	}

	@Override
	public int getNumJoueur() {
		return this.joueur;
	}

	private String getJoueurByNum(int num) {
		return num == BLANC ? "blanc" : "noir";
	}

	private String display_tab(String[] array) {
		StringBuilder res = new StringBuilder();
		for (int i = 0; i < array.length - 1; i++) {
			res.append(array[i]).append(" | ");
		}
		res.append(array[array.length - 1]);
		return res.toString();
	}

	@Override
	public String choixMouvement() {
		System.out.println("\nC'est � moi de jouer ! (" + this.couleur + ")" + "  lisere == " + this.myBoard.getLastDestLisere());
		this.myBoard.displayBoard();

		// Get last liseret : 1,2 => minimax; 3 => java.lang.StackOverflowError

		String bestCoup = "E";
		if (this.myBoard.getLastDestLisere() != 3) {
			// 3 algo possibles
			//bestCoup = this.minimax.meilleurCoup(this.myBoard);
			bestCoup = this.minimaxAB.meilleurCoup(this.myBoard);
			//bestCoup = this.minimaxv2.meilleurCoup(this.myBoard);
		} else {
			int indice = 0;
			String[] array = myBoard.possiblesMoves(this.couleur);
			System.out.println(array.length + " coups possibles : " + display_tab(array));
			// Choix du meilleur coup possible == se rapprocher le plus proche de la licorne adverse
			if (!myBoard.isInitialB && !myBoard.isInitialN && !array[0].equals("E")) {
				int maxValue = 0;
				for (int i = 0; i < array.length; i++) {
					String destPal = array[i].substring(3, 5);
					int eval = eval_heuristique(this, destPal);
					if (eval > maxValue) {
						maxValue = eval;
						indice = i;
					}
				}
				bestCoup = array[indice];
			}
		}

		System.out.println("choixMouvement renvoi le meilleur coup " + bestCoup);
		this.myBoard.play(bestCoup, this.couleur);
		return bestCoup;

	}

	public int eval_heuristique(JoueurSuperFort jsf, String myMove) {
		int dist_max = 10;
		int dist_PtoL;
		int h;

		// Localiser la Licorne adverse
		String licorne_E = localiseLicorneEnnemi(jsf);

		// Get distance between paladin & licorne adverse
		dist_PtoL = calculate_dist_paladin_LicorneEnnemi(myMove, licorne_E);

		h = dist_max - dist_PtoL;

		return h;
	}

	public String localiseLicorneEnnemi(JoueurSuperFort jsf) {
		char[][] board = jsf.myBoard.getBoard();
		char licorneEnnemi = this.couleur.equals("blanc") ? 'N' : 'B';
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				if (board[i][j] == licorneEnnemi) {
					return ("" + (char) ('A' + j) + (char) ('0' + i + 1));
				}
			}
		}
		return null;
	}

	public String localiseMaLicorne(JoueurSuperFort jsf) {
		char[][] board = jsf.myBoard.getBoard();
		char licorneEnnemi = this.couleur.equals("blanc") ? 'B' : 'N';
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				if (board[i][j] == licorneEnnemi) {
					return ("" + (char) ('A' + j) + (char) ('0' + i + 1));
				}
			}
		}
		return null;
	}

	public int calculate_dist_paladin_LicorneEnnemi(String case_p, String case_lic) {
		int pal_i = case_p.toCharArray()[0] - 'A';
		int pal_j = case_p.toCharArray()[1] - '0' - 1;
		int lic_i = case_lic.toCharArray()[0] - 'A';
		int lic_j = case_lic.toCharArray()[1] - '0' - 1;
		// Manhattan distance
		return (Math.abs(pal_i - lic_i) + Math.abs(pal_j - lic_j));
	}

	@Override
	public void declareLeVainqueur(int colour) {
		System.out.println("**********" + "  VICTOIRE DU JOUEUR " + getJoueurByNum(colour).toUpperCase() + "  ************");
		if(this.joueur == colour)
		{

		System.out.println("                                                                                 ,---,  \n" +
				"                   ,----,                                         ,--.        ,`--.' |  \n" +
				"    ,---,.       .'   .`|                   .---.   ,---,       ,--.'|        |   :  :  \n" +
				"  ,'  .' |    .'   .'   ;                  /. ./|,`--.' |   ,--,:  : |        '   '  ;  \n" +
				",---.'   |  ,---, '    .'              .--'.  ' ;|   :  :,`--.'`|  ' :        |   |  |  \n" +
				"|   |   .'  |   :     ./              /__./ \\ : |:   |  '|   :  :  | |        '   :  ;  \n" +
				":   :  |-,  ;   | .'  /           .--'.  '   \\' .|   :  |:   |   \\ | :        |   |  '  \n" +
				":   |  ;/|  `---' /  ;           /___/ \\ |    ' ''   '  ;|   : '  '; |        '   :  |  \n" +
				"|   :   .'    /  ;  /            ;   \\  \\;      :|   |  |'   ' ;.    ;        ;   |  ;  \n" +
				"|   |  |-,   ;  /  /--,           \\   ;  `      |'   :  ;|   | | \\   |        `---'. |  \n" +
				"'   :  ;/|  /  /  / .`|            .   \\    .\\  ;|   |  ''   : |  ; .'         `--..`;  \n" +
				"|   |    \\./__;       :             \\   \\   ' \\ |'   :  ||   | '`--'          .--,_     \n" +
				"|   :   .'|   :     .'               :   '  |--\" ;   |.' '   : |              |    |`.  \n" +
				"|   | ,'  ;   |  .'                   \\   \\ ;    '---'   ;   |.'              `-- -`, ; \n" +
				"`----'    `---'                        '---\"             '---'                  '---`\"  \n");
		} else {
			System.out.println("   ,--,                                                     \n" +
					",---.'|       ,----..       ,----..                         \n" +
					"|   | :      /   /   \\     /   /   \\   .--.--.       ,---,. \n" +
					":   : |     /   .     :   /   .     : /  /    '.   ,'  .' | \n" +
					"|   ' :    .   /   ;.  \\ .   /   ;.  \\  :  /`. / ,---.'   | \n" +
					";   ; '   .   ;   /  ` ;.   ;   /  ` ;  |  |--`  |   |   .' \n" +
					"'   | |__ ;   |  ; \\ ; |;   |  ; \\ ; |  :  ;_    :   :  |-, \n" +
					"|   | :.'||   :  | ; | '|   :  | ; | '\\  \\    `. :   |  ;/| \n" +
					"'   :    ;.   |  ' ' ' :.   |  ' ' ' : `----.   \\|   :   .' \n" +
					"|   |  ./ '   ;  \\; /  |'   ;  \\; /  | __ \\  \\  ||   |  |-, \n" +
					";   : ;    \\   \\  ',  /  \\   \\  ',  / /  /`--'  /'   :  ;/| \n" +
					"|   ,/      ;   :    /    ;   :    / '--'.     / |   |    \\ \n" +
					"'---'        \\   \\ .'      \\   \\ .'    `--'---'  |   :   .' \n" +
					"              `---`         `---`                |   | ,'   \n" +
					"                                                 `----' ");
		}
	}

	@Override
	public void mouvementEnnemi(String coup) {
		System.out.println("Le joueur advserse joue " + coup);
		this.myBoard.play(coup, this.couleur.equals("blanc") ? "noir" : "blanc");
	}

	@Override
	public String binoName() {
		return "Team Thibault Delorme & Dorian Verriere";
	}

	public static void main(String[] args) {

		JoueurSuperFort jsf_n = new JoueurSuperFort();
		JoueurSuperFort jsf_b = new JoueurSuperFort();
		jsf_n.initJoueur(NOIR);
		jsf_b.initJoueur(BLANC);
		jsf_n.myBoard.play("A1/A2/B2/C2/D2/E2", "noir"); // Noir
		jsf_n.myBoard.displayBoard();

		jsf_n.mouvementEnnemi("A6/B5/C5/D5/E5/F5"); // Blanc
		jsf_n.myBoard.displayBoard();

		jsf_n.mouvementEnnemi("A6-B4"); // Blanc
		jsf_n.myBoard.displayBoard();

		jsf_n.choixMouvement(); // Noir
		jsf_n.myBoard.displayBoard();
		jsf_n.declareLeVainqueur(BLANC);
	}

}
